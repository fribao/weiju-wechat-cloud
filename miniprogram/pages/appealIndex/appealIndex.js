// pages/appealIndex/appealIndex.js
import {
  pageAppeal
} from "../..//api/index"
import {
  format
} from "../../utils/util"
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabActive: 0,
    CustomBar: app.globalData.CustomBar,
    leftAppeal: [],
    rightAppeal: [],
    textFixed: "",
    pageData: {
      pageNO: 1,
      pageSize: 10,
      leftNum: 0,
      rightNum: 0
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.getAppealList()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  tabClick: function (e) {
    const tab = e.currentTarget.dataset.tab
    this.setData({
      tabActive: tab
    })
  },
  getImgInfo: function (url) {
    return new Promise((reslove, reject) => {
      wx.getImageInfo({
        src: url,
        success(res) {
          reslove({
            width: res.width,
            height: res.height,
          })
        },
        fail: function (err) {
          reject(err)
        }
      })
    })
  },
  getAppealList: async function () {
    let data = {
      pageNO: this.data.pageData.pageNO,
      pageSize: this.data.pageData.pageSize
    }

    console.log(123)
    const result = await pageAppeal(data)
    let leftNum = this.data.pageData.leftNum
    let rightNum = this.data.pageData.rightNum
    let left = []
    let right = []
    let asyncArr = []
    let titleArr = []
    for (let i in result) {
      this.setData({
        textFixed: result[i].title
      })
      result[i].createTime = format(result[i].createTime)
      asyncArr.push(this.getImgInfo(result[i].appealMaterial[0]))
      const query = wx.createSelectorQuery()
      query.select('#textFixed').boundingClientRect()
      query.exec(function (res) {
        titleArr.push(res[0].height)
      })
    }
    const appealImgsInfo = await Promise.all(asyncArr)
    for (let i in appealImgsInfo) {
      let height = (appealImgsInfo[i].height / appealImgsInfo[i].width) * 348 +titleArr[i]
      if (leftNum <= rightNum) {
        leftNum += height
        left.push(result[i])
      } else {
        rightNum += height
        right.push(result[i])
      }
    }
    this.setData({
      leftAppeal: left,
      rightAppeal: right,
      [`pageData.leftNum`]: leftNum,
      [`pageData.rightNum`]: rightNum
    })
    console.log(this.data.leftAppeal, this.data.rightAppeal)

  }
})