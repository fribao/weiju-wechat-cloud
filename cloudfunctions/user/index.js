// 云函数入口文件
const updateUser = require("./updateUser/index")
const updateAvatar = require("./updateAvatar/index")
const userInfo = require("./userInfo/index")
const userComment = require("./comment/index")

const cloud = require('wx-server-sdk')
cloud.init()
// 云函数入口函数
exports.main = async (event) => {
  const api = event.api //接口类型
  const updateData = event.data //所传递的数据
  let exportData = {}
  //错误码
  const responseCode = await cloud.callFunction({
    name: 'code'
  })
  if (api === 'update_user') {
    const userResult = await updateUser(updateData)
    exportData = Object.assign({
      data: userResult
    }, responseCode.result['00000'])
  } else if (api === 'update_user_avatar') {
    //更新用户的头像
    const avatarResult = await updateAvatar(updateData)
    exportData = Object.assign({
      data: avatarResult
    }, responseCode.result['00000'])
  }else if (api==='user_info'){
    const userInfoResult = await userInfo(updateData)
    exportData = Object.assign({
      data: userInfoResult
    }, responseCode.result['00000'])
  }else if(api==='user_comment'){
    const userCommentResult = await userComment(updateData)
    exportData = Object.assign({
      data: userCommentResult
    }, responseCode.result['00000'])
  }
  return exportData
}